defmodule Makina.Info do
  def generate(
        name,
        attributes,
        attribute_init,
        attribute_type,
        commands,
        command_declaration,
        command_callbacks
      ) do
    quote do
      def __model_info__ do
        %{
          name: unquote(name),
          attributes: unquote(attributes),
          attribute_init: unquote(Macro.escape(attribute_init)),
          attribute_type: unquote(Macro.escape(attribute_type)),
          commands: unquote(commands),
          command_declaration: unquote(Macro.escape(command_declaration)),
          command_callbacks: unquote(Macro.escape(command_callbacks))
        }
      end
    end
  end
end
