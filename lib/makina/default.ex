defmodule Makina.Default do
  import Makina.Lib

  def state(_attributes, initial_state) do
    quote do
      def initial_state, do: Map.new(unquote(initial_state))
    end
  end

  def command(command) do
    for callback <- default_callbacks() do
      default_impl(callback, command) |> get_block
    end
    |> Enum.concat()
  end

  defp default_impl(:pre, _declaration) do
    quote do
      def pre(_state), do: true
      defoverridable(pre: 1)
    end
  end

  defp default_impl(:valid_args, _declaration) do
    quote do
      def valid_args(_state, _args), do: true
      defoverridable(valid_args: 2)
    end
  end

  defp default_impl(:next, _declaration) do
    quote do
      def next(state, _args, _result), do: state
      defoverridable(next: 3)
    end
  end

  defp default_impl(:post, _declaration) do
    quote do
      def post(_state, _args, _res), do: true
      defoverridable(post: 3)
    end
  end

  defp default_impl(:features, _declaration) do
    quote do
      def features(_state, _args, _res), do: []
      defoverridable(features: 3)
    end
  end

  defp default_impl(:adapt, _declaration) do
    quote do
      def adapt(_state, _args), do: false
      defoverridable(adapt: 2)
    end
  end

  defp default_impl(:dynamicpre, _declaration) do
    quote do
      def dynamicpre(_state, _args), do: true
      defoverridable(dynamicpre: 2)
    end
  end

  defp default_impl(:wrap_args, declaration) do
    args = Macro.prewalk(declaration, &remove_types/1) |> elem(2)
    wrapped_args = gen_map_args(args)

    quote do
      def wrap_args(unquote(args)), do: unquote(wrapped_args)
    end
  end

  defp default_impl(:unwrap_args, declaration) do
    args = Macro.prewalk(declaration, &remove_types/1) |> elem(2)
    wrapped_args = gen_map_args(args)

    quote do
      def unwrap_args(unquote(wrapped_args)), do: unquote(args)
    end
  end

  defp default_impl(_callback, _declaration) do
    quote do
    end
  end
end
