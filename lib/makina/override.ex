defmodule Makina.Override do
  import Makina.Lib

  def command(command, state_attributes, block) do
    # name = get_command_id(command)
    args = Macro.prewalk(command, &remove_types/1) |> elem(2)

    insert_implicit_args(args, state_attributes, get_block(block))
    |> get_block
    |> Enum.concat()
  end

  defp insert_implicit_args(args, state_attributes, block) do
    {_, map_args} = Macro.prewalk(args, [], &get_vars/2)

    Macro.postwalk(block, fn ast ->
      ast
      # state map
      |> insert_map_arg_at(:pre, 0, state_attributes, :state)
      |> insert_map_arg_at(:args, 0, state_attributes, :state)
      |> insert_map_arg_at(:valid_args, 0, state_attributes, :state)
      |> insert_map_arg_at(:post, 0, state_attributes, :state)
      |> insert_map_arg_at(:next, 0, state_attributes, :state)
      # arguments map
      |> insert_map_arg_at(:valid_args, 1, map_args, :arguments)
      |> insert_map_arg_at(:post, 1, map_args, :arguments)
      |> insert_map_arg_at(:next, 1, map_args, :arguments)
      # result
      |> insert_arg_at(:post, 2, :result)
      |> insert_arg_at(:next, 1, :result)
    end)
  end

  ##############################################################################
  # helpers
  ##############################################################################

  defp insert_arg_at({:def, info0, [{name, info1, args}, block]}, function, index, arg)
       when function == name and is_atom(arg) do
    {_, vars} = Macro.postwalk(block, [], &get_vars/2)

    arg = if arg in vars, do: :"#{arg}", else: :"_#{arg}"

    if args == nil do
      {:def, info0, [{name, info1, [Macro.var(arg, nil)]}, block]}
    else
      {:def, info0, [{name, info1, List.insert_at(args, index, Macro.var(arg, nil))}, block]}
    end
  end

  defp insert_arg_at(ast, _function, _index, _arg), do: ast

  defp insert_map_arg_at(
         {:def, info0, [{name, info1, fun_args}, block]},
         function,
         index,
         args,
         arg_name
       )
       when function == name and is_list(args) do
    {_, vars} = Macro.postwalk(block, [], &get_vars/2)

    map_args =
      for key <- args do
        value = if key in vars, do: Macro.var(key, nil), else: Macro.var(:"_#{key}", nil)
        {key, value}
      end

    map_args_pattern = {:%{}, [], map_args}

    arguments_pattern =
      if arg_name in vars do
        {:=, [], [Macro.var(arg_name, nil), map_args_pattern]}
      else
        map_args_pattern
      end

    cond do
      fun_args == nil ->
        {:def, info0, [{name, info1, [arguments_pattern]}, block]}

      not is_list(fun_args) ->
        {:def, info0,
         [{name, info1, List.insert_at([fun_args], index, arguments_pattern)}, block]}

      true ->
        {:def, info0, [{name, info1, List.insert_at(fun_args, index, arguments_pattern)}, block]}
    end
  end

  defp insert_map_arg_at(ast, _function, _index, _arg, _arg_name), do: ast

  @known_suffixes known_suffixes()

  def rename_def_in_command({:def, c1, [{:call, c2, impl_args}, impl_body]}, name) do
    {:def, c1, [{name, c2, impl_args}, impl_body]}
  end

  def rename_def_in_command({:defdelegate, c1, [{:call, c2, args}, spec]}, name) do
    spec =
      if spec[:as] do
        spec
      else
        spec ++ [as: name]
      end

    {:defdelegate, c1, [{name, c2, args}, spec]}
  end

  def rename_def_in_command({:defdelegate, c1, [{suffix_name, c2, args}, spec]}, name)
      when suffix_name in @known_suffixes do
    new_name = :"#{name}_#{suffix_name}"
    {:defdelegate, c1, [{new_name, c2, args}, spec]}
  end

  def rename_def_in_command({:def, c1, [{suffix_name, c2, args}, body]}, name)
      when suffix_name in @known_suffixes do
    new_name = :"#{name}_#{suffix_name}"
    {:def, c1, [{new_name, c2, args}, body]}
  end

  def rename_def_in_command({:spec, c1, [{:"::", c2, [{:call, c3, values}, type]}]}, name) do
    {:spec, c1, [{:"::", c2, [{name, c3, values}, type]}]}
  end

  def rename_def_in_command({:spec, c1, [{:"::", c2, [{suffix_name, c3, values}, type]}]}, name)
      when suffix_name in @known_suffixes do
    new_name = :"#{name}_#{suffix_name}"
    {:spec, c1, [{:"::", c2, [{new_name, c3, values}, type]}]}
  end

  def rename_def_in_command({:defoverridable, c1, [suffixes]}, name) do
    new_names =
      for {suffix, arity} <- suffixes, suffix in [:call | @known_suffixes] do
        if suffix == :call, do: {:"#{name}", arity}, else: {:"#{name}_#{suffix}", arity}
      end

    {:defoverridable, c1, [new_names]}
  end

  def rename_def_in_command(ast, _name), do: ast
end
