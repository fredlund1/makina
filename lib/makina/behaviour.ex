defmodule Makina.Behaviour do
  def generate(commands) do
    [
      statem_functions(),
      command_weights(commands),
      pre_internal(commands),
      args_internal(commands),
      valid_args_internal(commands),
      post_internal(commands),
      args_internal(commands),
      next_internal(commands),
      wrap_args_internal(commands),
      unwrap_args_internal(commands),
      features_internal(commands),
      adapt_internal(commands),
      dynamic_preconditions_internal(commands)
    ]
  end

  # generates eqc callbacks
  defp statem_functions do
    quote do
      defp call_command({:call, _m, command, _args}), do: command
      defp call_command({:call, _m, command, _args, _meta}), do: command

      defp call_args({:call, _m, _command, args}), do: args
      defp call_args({:call, _m, _command, args, _Meta}), do: args

      # Define a set of standard functions with call arguments,
      # extracting the commands and arguments from them, and
      # call the internally generated functions.

      def command_gen(state) do
        weighted_command = frequency(weights_int(state))

        viable_command =
          such_that(command <- weighted_command, when: pre_command_int(command, state))

        let cmd <- viable_command do
          case args_int(cmd, state) do
            args when is_map(args) ->
              IO.puts("#{inspect cmd}: got args #{inspect args} which is a map")
              {cmd, unwrap_args_int(cmd, args)}
            gen -> let args <- gen do
                IO.puts("#{inspect cmd}: got args #{inspect args} which is not a map; after gen #{inspect gen}")
                {cmd, unwrap_args_int(cmd, args)}
              end
          end
        end
      end

      def command(state) do
        let({cmd, args} <- command_gen(state), do: {:call, __MODULE__, cmd, args})
      end

      def next_state(state, res, call) do
        args = wrap_args_int(call_command(call), call_args(call))
        next_int(call_command(call), state, res, args)
      end

      def postcondition(state, call, res) do
        args = wrap_args_int(call_command(call), call_args(call))
        post_int(call_command(call), state, args, res)
      end

      def precondition(state, call) do
        args = wrap_args_int(call_command(call), call_args(call))
        valid_args_int(call_command(call), state, args)
      end

      def call_features(state, call, res) do
        args = wrap_args_int(call_command(call), call_args(call))

        features_int(
          call_command(call),
          state,
          args,
          res
        )
      end

      def adapt(state, call) do
        args = wrap_args_int(call_command(call), call_args(call))
        adapt_int(call_command(call), state, args)
      end

      def dynamic_precondition(state, call) do
        args = wrap_args_int(call_command(call), call_args(call))

        dynamicpre_int(
          call_command(call),
          state,
          args
        )
      end

      def weight(_state, _command), do: 1

      defoverridable command_gen: 1, weight: 2
    end
  end

  defp gen_int_fun(name, commands, args, int_fun_name \\ nil) do
    var_args = Enum.map(args, &make_var/1)
    int_fun = int_fun_name || :"#{name}_int"

    for cmd_name <- commands do
      quote do
        def unquote(int_fun)(unquote_splicing([cmd_name | var_args])) do
          unquote(:"#{cmd_name}_#{name}")(unquote_splicing(var_args))
        end
      end
    end
  end

  defp make_var(atom) do
    Macro.var(atom, __MODULE__)
  end

  # Return a list [{weight, command},...] of commands with assigned weights
  def command_weights(commands) do
    quote do
      defp weights_int(state) do
        unquote(
          for cmd_name <- commands do
            quote do
              {weight(state, unquote(cmd_name)), unquote(cmd_name)}
            end
          end
        )
      end
    end
  end

  # Define a function, e.g, post_int(command, ...) which serves as a bridge
  # between the command_post(...) function and the postcondition function.

  defp pre_internal(commands), do: gen_int_fun(:pre, commands, [:state], :pre_command_int)
  defp args_internal(commands), do: gen_int_fun(:args, commands, [:state])
  defp valid_args_internal(commands), do: gen_int_fun(:valid_args, commands, [:state, :args])
  defp post_internal(commands), do: gen_int_fun(:post, commands, [:state, :args, :res])
  defp next_internal(commands), do: gen_int_fun(:next, commands, [:state, :res, :args])
  defp wrap_args_internal(commands), do: gen_int_fun(:wrap_args, commands, [:args])

  defp unwrap_args_internal(commands),
    do: gen_int_fun(:unwrap_args, commands, [:args])

  defp features_internal(commands), do: gen_int_fun(:features, commands, [:state, :args, :res])
  defp adapt_internal(commands), do: gen_int_fun(:adapt, commands, [:state, :args])

  defp dynamic_preconditions_internal(commands),
    do: gen_int_fun(:dynamicpre, commands, [:state, :args])
end
