defmodule Makina.Lib do
  ##############################################################################
  # info
  ##############################################################################

  @eqc_functions [
    pre: 1,
    args: 1,
    valid_args: 2,
    post: 3,
    next: 3,
    features: 3,
    adapt: 2,
    dynamicpre: 2,
    wrap_args: 1,
    unwrap_args: 1
  ]

  @default_callbacks [
    :pre,
    :valid_args,
    :next,
    :post,
    :features,
    :adapt,
    :dynamicpre,
    :wrap_args,
    :unwrap_args,
    :call
  ]

  @known_suffixes Keyword.keys(@eqc_functions) |> Enum.uniq()

  for suffix <- @known_suffixes do
    def unquote(suffix)(name), do: :"#{name}_#{unquote(suffix)}"
  end

  def eqc_functions, do: @eqc_functions

  def known_suffixes, do: @known_suffixes

  def default_callbacks, do: @default_callbacks

  ##############################################################################
  # helpers
  ##############################################################################

  def overridable(functions) do
    quote do
      defoverridable unquote(functions)
    end
  end

  ##############################################################################
  # ast
  ##############################################################################

  def get_command_args({:"::", _info, [{_name, _info2, args}, _return_type]}), do: args

  def get_command_return_type({:"::", _info, [{_name, _info2, _args}, return_type]}),
    do: return_type

  def get_command_id(command) do
    name = get_command_name(command)
    # arity = get_arity(command)
    # :"#{name}_#{arity}"
    :"#{name}"
  end

  def get_command_name({:"::", _info, [{name, _info2, _args}, _return_type]}), do: name
  def get_command_name({name, _, _}), do: name

  def get_command_arity({:"::", _info, [{_name, _info2, args}, _return_type]}), do: length(args)
  def get_command_arity({_name, _, args}), do: length(args)

  ##############################################################################
  # ast helpers
  ##############################################################################

  def get_vars(ast = {name, _info, nil}, acc), do: {ast, [name | acc]}
  def get_vars(ast = {name, _info, mod}, acc) when is_atom(mod), do: {ast, [name | acc]}
  def get_vars(ast, acc), do: {ast, acc}

  def get_types({:"::", _info, [_value, type]}), do: type
  def get_types(ast), do: ast

  def remove_types({:"::", _info, [value, _type]}), do: value
  def remove_types(ast), do: ast

  def get_defs(ast = {:def, _info, [{name, _, args} | _]}, acc) when is_list(args),
    do: {ast, [{name, length(args)} | acc]}

  def get_defs(ast = {:def, _info, [{name, _, _args} | _]}, acc) do
    {ast, [{name, 0} | acc]}
  end

  def get_defs(ast = {:defdelegate, _info, [{name, _, args} | _]}, acc) when is_list(args) do
    {ast, [{name, length(args)} | acc]}
  end

  def get_defs(ast = {:defdelegate, _info, [{name, _, _args} | _]}, acc) do
    {ast, [{name, nil} | acc]}
  end

  def get_defs(ast, acc), do: {ast, acc}

  def get_block({:__block__, _info, block}), do: block
  def get_block(block), do: [block]

  ##############################################################################
  # module info getters
  ##############################################################################

  def commands(module), do: module.__model_info__[:commands]
  def attributes(module), do: module.__model_info__[:attributes]

  def attribute_init(module, attribute), do: module.__model_info__[:attribute_init][attribute]
  def attribute_type(module, attribute), do: module.__model_info__[:attribute_type][attribute]

  def command_declaration(module, cmd), do: module.__model_info__[:command_declaration][cmd]
  def command_callbacks(module, cmd), do: module.__model_info__[:command_callbacks][cmd]

  def command_arity(module, cmd), do: module.__model_info__[:command_callbacks][cmd][:call]

  ##############################################################################
  # module info predicates
  ##############################################################################

  def defines(module, cmd), do: cmd in commands(module)

  def defines(module, cmd, callback) do
    command_callbacks = module.__model_info__[:command_callbacks][cmd] |> Keyword.keys()
    defines(module, cmd) and callback in command_callbacks
  end

  ##############################################################################
  # helpers
  ############################################################################## a

  def gen_map_args(args) do
    {:%{}, [],
     for arg = {name, _info, _env} <- args do
       {name, arg}
     end}
  end
end
