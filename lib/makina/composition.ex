# TODO type check attributes are compatible

defmodule Makina.Composition do
  import Makina.Lib

  alias Makina.Behaviour
  alias Makina.Types
  alias Makina.Default
  alias Makina.Info

  def generate(modules, env, checker) do
    modules = for module <- modules, do: Macro.expand(module, env)
    composed_module = :"#{env.module}.ComposedModel"

    code = [
      generate_state(modules),
      generate_commands(modules),
      generate_info(modules)
    ]

    quote do
      defmodule unquote(composed_module) do
        use unquote(checker)

        import Makina.Lib

        unquote_splicing(code)
      end
    end
  end

  defp generate_state(modules) do
    {inits, types} =
      for module <- modules, attribute <- attributes(module), reduce: {%{}, %{}} do
        {inits, types} ->
          init = attribute_init(module, attribute)
          type = attribute_type(module, attribute)

          {
            Map.put(inits, attribute, init),
            Map.put(types, attribute, type)
          }
      end

    types = types |> Map.new()
    inits = inits |> Map.new() |> Keyword.new()

    Types.state([], types) ++ [Default.state([], inits)]
  end

  defp generate_commands(modules) do
    commands =
      for module <- modules, command = command <- commands(module), reduce: [] do
        acc -> [command | acc]
      end

    commands_code =
      for command <- commands |> Enum.uniq() do
        modules = Enum.filter(modules, &defines(&1, command))
        generate_command(command, modules)
      end

    Behaviour.generate(commands) ++ commands_code
  end

  defp generate_command(command, modules) do
    for callback <- default_callbacks() ++ [:args, :call] do
      generate_callback(callback, command, modules)
    end
  end

  defp generate_callback(:pre, command, modules) do
    modules = Enum.filter(modules, &(defines(&1, command, :pre) and defines(&1, command, :args)))

    inner_exprs =
      for module <- modules do
        quote do
          unquote(module).unquote(pre(command))(state)
        end
      end

    disjunction =
      for expr <- inner_exprs, reduce: [] do
        [] -> expr
        acc -> {:or, [], [expr, acc]}
      end

    quote do
      def unquote(pre(command))(state), do: unquote(disjunction)
    end
  end

  defp generate_callback(:args, command, modules) do
    modules = Enum.filter(modules, &(defines(&1, command, :args) and defines(&1, command, :pre)))

    quote do
      def unquote(args(command))(state) do
        module = Enum.random(unquote(modules))
        module.unquote(args(command))(state)
      end
    end
  end

  defp generate_callback(:call, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :call))

    args = Macro.generate_arguments(command_arity(hd(modules), command), nil)

    quote do
      def unquote(command)(unquote_splicing(args)) do
        module = Enum.random(unquote(modules))
        module.unquote(command)(unquote_splicing(args))
      end
    end
  end

  defp generate_callback(:valid_args, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :valid_args))

    inner_exprs =
      for module <- modules do
        quote do
          unquote(module).unquote(valid_args(command))(state, arguments)
        end
      end

    conjunction =
      for expr <- inner_exprs, reduce: [] do
        [] -> expr
        acc -> {:and, [], [expr, acc]}
      end

    quote do
      def unquote(valid_args(command))(state, arguments), do: unquote(conjunction)
    end
  end

  defp generate_callback(:post, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :post))

    inner_exprs =
      for module <- modules do
        quote do
          unquote(module).unquote(post(command))(state, arguments, result)
        end
      end

    conjunction =
      for expr <- inner_exprs, reduce: [] do
        [] -> expr
        acc -> {:and, [], [expr, acc]}
      end

    quote do
      def unquote(post(command))(state, arguments, result), do: unquote(conjunction)
    end
  end

  defp generate_callback(:next, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :next))

    inner_exprs =
      for module <- modules do
        quote do
          unquote(module).unquote(next(command))(result, arguments)
        end
      end

    pipe =
      for expr <- inner_exprs, reduce: Macro.var(:state, __MODULE__) do
        acc -> {:|>, [], [acc, expr]}
      end

    quote do
      def unquote(next(command))(state, result, arguments), do: unquote(pipe)
    end
  end

  defp generate_callback(:wrap_args, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :wrap_args))
    declaration = command_declaration(hd(modules), command)
    args = Macro.prewalk(declaration, &remove_types/1) |> elem(2)
    wrapped_args = gen_map_args(args)

    quote do
      def unquote(wrap_args(command))(unquote(args)), do: unquote(wrapped_args)
    end
  end

  defp generate_callback(:unwrap_args, command, modules) do
    modules = Enum.filter(modules, &defines(&1, command, :wrap_args))
    declaration = command_declaration(hd(modules), command)
    args = Macro.prewalk(declaration, &remove_types/1) |> elem(2)
    wrapped_args = gen_map_args(args)

    quote do
      def unquote(unwrap_args(command))(unquote(wrapped_args)), do: unquote(args)
    end
  end

  defp generate_callback(:features, command, _modules) do
    quote do
      def unquote(features(command))(_state, _args, _res), do: []
    end
  end

  defp generate_callback(:adapt, command, _modules) do
    quote do
      def unquote(adapt(command))(_state, _args), do: true
    end
  end

  defp generate_callback(:dynamicpre, command, _modules) do
    quote do
      def unquote(dynamicpre(command))(_state, _args), do: true
    end
  end

  defp generate_callback(_callback, _command, _modules) do
    []
  end

  defp generate_info(modules) do
    attributes =
      for module <- modules, attribute <- attributes(module), reduce: [] do
        acc -> [attribute | acc]
      end
      |> Enum.uniq()

    attribute_init =
      for module <- modules, attribute <- attributes(module), reduce: [] do
        acc -> [{attribute, attribute_init(module, attribute)} | acc]
      end
      |> Map.new()

    attribute_type =
      for module <- modules, attribute <- attributes(module), reduce: [] do
        acc -> [{attribute, attribute_type(module, attribute)} | acc]
      end
      |> Map.new()

    commands =
      for module <- modules, command <- commands(module), reduce: [] do
        acc -> [command | acc]
      end
      |> Enum.uniq()

    command_declaration =
      for module <- modules, command <- commands(module), reduce: [] do
        acc -> [{command, command_declaration(module, command)} | acc]
      end
      |> Map.new()

    command_callbacks =
      for module <- modules, command <- commands(module), reduce: [] do
        acc -> [{command, command_callbacks(module, command)} | acc]
      end
      |> Map.new()

    [
      Info.generate(
        ComposedModel,
        attributes,
        attribute_init,
        attribute_type,
        commands,
        command_declaration,
        command_callbacks
      )
    ]
  end
end
