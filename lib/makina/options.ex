defmodule Makina.Options do
  import Makina.Lib

  def implemented_by(_command, nil), do: []

  def implemented_by(command, module) do
    arity = get_command_arity(command)
    [delegate_function(:call, arity, module), overridable([{:call, arity}])]
  end

  def extends_pre_block(_command, nil), do: []

  def extends_pre_block(command, module) do
    name = get_command_id(command)

    if name in commands(module) do
      for {callback, arity} <- command_callbacks(module, name) do
        [delegate_function(callback, arity, module), overridable([{callback, arity}])]
      end
    else
      []
    end
  end

  def extends_post_block(_command, nil, _callbacks), do: []

  def extends_post_block(command, module, callbacks) do
    name = get_command_id(command)

    if name in commands(module) do
      for callback <- callbacks, defines(module, name, callback) do
        post_block(callback, module, name)
      end
    else
      []
    end
  end

  defp post_block(:post, module, name) do
    quote do
      defoverridable([{:post, 3}])

      def post(state, arguments, result) do
        super(state, arguments, result) and
          unquote({:., [], [module, post(name)]})(state, arguments, result)
      end
    end
  end

  defp post_block(_callback, _module, _name) do
    []
  end

  defp delegate_function(name, arity, module) do
    args = Macro.generate_arguments(arity, nil)

    quote do
      defdelegate unquote(name)(unquote_splicing(args)),
        to: unquote(module)
    end
  end
end
