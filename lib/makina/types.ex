defmodule Makina.Types do
  import Makina.Lib

  def state(_attributes, types) do
    optional = [{{:optional, [], [{:any, [], nil}]}, {:any, [], nil}}]

    state_map_type = {:%{}, [], Enum.reverse(types) ++ optional}

    quote do
      @type state :: unquote(state_map_type)
      @spec initial_state :: state
    end
    |> elem(2)
  end

  def command(_declaration, nil), do: []

  def command(declaration, callbacks) do
    for callback <- (callbacks ++ default_callbacks()) |> Enum.uniq() do
      gen_type(callback, declaration)
    end
  end

  defp gen_type(:pre, _declaration) do
    quote do
      @spec pre(state) :: bool
    end
  end

  defp gen_type(:args, command) do
    args = get_command_args(command)
    map_args = Macro.prewalk(args, &gen_map_type/1)
    args_return_type = Macro.postwalk(map_args, &types_to_any/1)

    quote do
      @spec args(state) :: unquote(args_return_type)
    end
  end

  defp gen_type(:valid_args, command) do
    args = get_command_args(command)
    map_args = Macro.prewalk(args, &gen_map_type/1)

    quote do
      @spec valid_args(state, unquote(map_args)) :: bool
    end
  end

  defp gen_type(:post, command) do
    args = get_command_args(command)
    return_type = get_command_return_type(command)
    map_args = Macro.prewalk(args, &gen_map_type/1)

    quote do
      @spec post(state, unquote(map_args), unquote(return_type)) :: bool
    end
  end

  defp gen_type(:next, command) do
    args = get_command_args(command)
    return_type = get_command_return_type(command)
    map_args = Macro.prewalk(args, &gen_map_type/1)

    quote do
      @spec next(state, unquote(return_type), unquote(map_args)) :: state
    end
  end

  # defp gen_type(:features, command) do
  #   args = get_command_args(command)
  #   return_type = get_command_return_type(command)
  #   map_args = Macro.prewalk(args, &gen_map_type/1)

  #   quote do
  #     @spec features(state, unquote(return_type), unquote(map_args)) :: []
  #   end
  # end

  # defp gen_type(:adapt, command) do
  #   args = get_command_args(command)
  #   map_args = Macro.prewalk(args, &gen_map_type/1)

  #   quote do
  #     @spec adapt(state, unquote(map_args)) :: any
  #   end
  # end

  # defp gen_type(:dynamic_pre, command) do
  #   args = get_command_args(command)
  #   map_args = Macro.prewalk(args, &gen_map_type/1)

  #   quote do
  #     @spec dynamicpre(state, unquote(map_args)) :: Bool.t()
  #   end
  # end

  defp gen_type(:call, command) do
    args = get_command_args(command)
    return_type = get_command_return_type(command)

    quote do
      @spec call(unquote_splicing(args)) :: unquote(return_type)
    end
  end

  defp gen_type(_callback, _declaration) do
    []
  end

  ##############################################################################
  # inital state macro
  ##############################################################################

  defp types_to_any({key, {_value, info, env}}), do: {key, {:any, info, env}}
  defp types_to_any(ast), do: ast

  defp gen_map_type([]), do: {:%{}, [], []}

  defp gen_map_type({:"::", _, [{key, _, _}, type]}), do: {key, type}

  defp gen_map_type(ast) do
    if is_list(ast) and Enum.reduce(ast, fn x, acc -> match?({:"::", _, _}, x) and acc end) do
      {:%{}, [], ast}
    else
      ast
    end
  end
end
