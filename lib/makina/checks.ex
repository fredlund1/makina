defmodule Makina.Checks do
  import Makina.Lib

  def command({:"::", _info, [{_name, _info2, arguments}, _return_type]}) do
    args_without_types = Macro.prewalk(arguments, &remove_types/1)

    for {argument, count} <- Enum.frequencies(args_without_types), count != 1 do
      raise(%Makina.Error{
        message: "repeated argument \"#{Macro.to_string(argument)}\" in command definition"
      })
    end
  end

  def command(_command) do
    raise(%Makina.Error{
      message: "invalid command definition"
    })
  end

  def state(state) do
    if not Keyword.keyword?(state) do
      raise(%Makina.Error{
        message: "state definition is expected to be a keyword list"
      })
    end
  end

  def arg_number({name, _info, args} = ast, function, n_of_args)
      when function == name do
    case args do
      nil when n_of_args != 0 ->
        raise(%Makina.Error{
          message: "function #{name} expected #{n_of_args} arguments, but has 0"
        })

      _ when length(args) != n_of_args ->
        raise(%Makina.Error{
          message: "function #{name} expected #{n_of_args} arguments, but has #{length(args)}"
        })

      _ ->
        ast
    end
  end

  def arg_number(ast, _function, _n_of_args), do: ast
end
