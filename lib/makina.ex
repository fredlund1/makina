# TODO composed module should have access to types in previous modules
# TODO notify error when overriden attributes do not have equal type
# TODO notify error when overriden commands do not have equal types
# TODO notify error in parallel composition when attributes have different types
# TODO notify error in parallel composition when commands have different argument types

defmodule Makina do
  import Makina.Lib

  require Logger

  alias Makina.Behaviour
  alias Makina.Composition
  alias Makina.Checks
  alias Makina.Types
  alias Makina.Default
  alias Makina.Options
  alias Makina.Override
  alias Makina.Info

  defmacro __using__(options) do
    checker = Application.get_env(:makina, :checker)
    if is_nil(checker), do: raise(%Makina.Error{message: "checker not found"})

    # state
    Module.register_attribute(__CALLER__.module, :attributes, accumulate: true)
    Module.register_attribute(__CALLER__.module, :attribute_init, accumulate: true)
    Module.register_attribute(__CALLER__.module, :attribute_type, accumulate: true)
    # commands
    Module.register_attribute(__CALLER__.module, :commands, accumulate: true)
    Module.register_attribute(__CALLER__.module, :command_declaration, accumulate: true)
    Module.register_attribute(__CALLER__.module, :command_callbacks, accumulate: true)
    Module.register_attribute(__CALLER__.module, :command_options, accumulate: true)
    Module.register_attribute(__CALLER__.module, :command_block, accumulate: true)
    # options
    Module.register_attribute(__CALLER__.module, :implemented_by, accumulate: false)
    Module.register_attribute(__CALLER__.module, :composition, accumulate: false)
    Module.register_attribute(__CALLER__.module, :extends, accumulate: false)
    # lines
    Module.register_attribute(__CALLER__.module, :lines, accumulate: true)

    line = Macro.Env.location(__CALLER__)[:line]
    Module.put_attribute(__CALLER__.module, :lines, {:use, line})

    if :implemented_by in Keyword.keys(options) do
      implemented_by = Macro.expand(options[:implemented_by], __CALLER__)
      Module.put_attribute(__CALLER__.module, :implemented_by, implemented_by)
    end

    extends =
      cond do
        :extends in Keyword.keys(options) and is_list(options[:extends]) ->
          composed_module = :"#{__CALLER__.module}.ComposedModel"
          composed_code = Composition.generate(options[:extends], __CALLER__, checker)
          Macro.to_string(composed_code) |> Code.format_string!() |> Logger.debug()

          Code.eval_quoted(composed_code, [],
            file: __CALLER__.file,
            line: __CALLER__.line,
            module: composed_module
          )

          composed_module

        :extends in Keyword.keys(options) ->
          Macro.expand(options[:extends], __CALLER__)

        true ->
          nil
      end

    if extends != nil do
      for attribute <- attributes(extends) do
        init = {attribute, attribute_init(extends, attribute)}
        type = {attribute, attribute_type(extends, attribute)}
        Module.put_attribute(__CALLER__.module, :attributes, attribute)
        Module.put_attribute(__CALLER__.module, :attribute_init, init)
        Module.put_attribute(__CALLER__.module, :attribute_type, type)
      end

      for command <- commands(extends) do
        declaration = command_declaration(extends, command)
        callbacks = command_callbacks(extends, command)
        Module.put_attribute(__CALLER__.module, :commands, get_command_id(declaration))
        Module.put_attribute(__CALLER__.module, :command_declaration, {command, declaration})
        Module.put_attribute(__CALLER__.module, :command_callbacks, {command, callbacks})
      end

      Module.put_attribute(__CALLER__.module, :extends, extends)
    end

    quote do
      use unquote(checker)
      import unquote(__MODULE__)

      @before_compile unquote(__MODULE__)
    end
  end

  ##############################################################################
  # defstate macro
  ##############################################################################

  # unsafe (does not recursively check that the list is a keyword)
  defguardp is_keyword(list) when is_list(list) and is_tuple(hd(list))

  defmacro defstate(state) when is_keyword(state) and state != [] do
    Checks.state(state)

    line = Macro.Env.location(__CALLER__)[:line]

    get_type = fn
      {:"::", _, [_, type]} -> type
      _ -> {:any, [], nil}
    end

    get_value = fn
      {:"::", _, [value, _]} -> value
      value -> value
    end

    attributes = Enum.map(state, fn {key, _ast} -> key end)
    attributes_inits = Enum.map(state, fn {key, ast} -> {key, get_value.(ast)} end)
    attributes_types = Enum.map(state, fn {key, ast} -> {key, get_type.(ast)} end)

    for key <- attributes do
      Module.put_attribute(__CALLER__.module, :attributes, key)
      Module.put_attribute(__CALLER__.module, :attribute_init, {key, attributes_inits[key]})
      Module.put_attribute(__CALLER__.module, :attribute_type, {key, attributes_types[key]})
    end

    Module.put_attribute(__CALLER__.module, :lines, {:state, line})
  end

  defmacro defstate(state) do
    quote do
      defstate(state: unquote(state))
    end
  end

  ##############################################################################
  # defcommand macro
  ##############################################################################

  defmacro defcommand(command) do
    quote do
      defcommand(unquote(command), do: [])
    end
  end

  defmacro defcommand(
             {:"::", info1, [{name, info2, arguments}, return_type]},
             do: block
           ) do
    args_with_types =
      Enum.map(arguments, fn
        argument = {:"::", _, _} -> argument
        argument -> {:"::", [], [argument, {:any, [], nil}]}
      end)

    command = {:"::", info1, [{name, info2, args_with_types}, return_type]}

    Checks.command(command)

    arity = get_command_arity(command)
    command_id = get_command_id(command)

    Macro.postwalk(block, fn ast ->
      for function <- Keyword.keys(eqc_functions()) do
        Checks.arg_number(ast, function, 0)
      end

      Checks.arg_number(ast, :call, arity)
    end)

    line = Macro.Env.location(__CALLER__)[:line]

    callbacks =
      Macro.postwalk(block, [], &get_defs/2)
      |> elem(1)
      |> Enum.map(&elem(&1, 0))

    Module.put_attribute(__CALLER__.module, :commands, command_id)

    Module.put_attribute(__CALLER__.module, :command_declaration, {command_id, command})
    Module.put_attribute(__CALLER__.module, :command_callbacks, {command_id, callbacks})
    Module.put_attribute(__CALLER__.module, :command_block, {command_id, block})
    Module.put_attribute(__CALLER__.module, :lines, {command_id, line})
  end

  defmacro defcommand(command, block) do
    quote do
      defcommand(unquote(command) :: any, do: unquote(block))
    end
  end

  ##############################################################################
  # before compile
  ##############################################################################

  defmacro __before_compile__(env) do
    # state
    attributes = Module.get_attribute(env.module, :attributes) |> Enum.uniq()
    attribute_init = Module.get_attribute(env.module, :attribute_init) |> Enum.reverse()
    attribute_type = Module.get_attribute(env.module, :attribute_type) |> Enum.reverse()
    # commands
    commands = Module.get_attribute(env.module, :commands) |> Enum.uniq()
    command_declaration = Module.get_attribute(env.module, :command_declaration)
    command_callbacks = Module.get_attribute(env.module, :command_callbacks)
    # command_options = Module.get_attribute(env.module, :command_options)
    command_block = Module.get_attribute(env.module, :command_block)
    # options
    implemented_by = Module.get_attribute(env.module, :implemented_by)
    extends = Module.get_attribute(env.module, :extends)
    # info
    lines = Module.get_attribute(env.module, :lines)

    behaviour = Behaviour.generate(commands)

    state =
      (Types.state(attributes, attribute_type) ++ [Default.state(attributes, attribute_init)])
      |> Macro.prewalk(&fix_location(&1, lines[:state]))

    commands_result =
      for command_id <- commands, reduce: [] do
        acc ->
          declaration = command_declaration[command_id]

          callbacks =
            command_callbacks[command_id] ++ if implemented_by != nil, do: [:call], else: []

          block = command_block[command_id]

          types = Types.command(declaration, callbacks)
          default = Default.command(declaration)
          implemented_by = Options.implemented_by(declaration, implemented_by)
          extends_pre = Options.extends_pre_block(declaration, extends)
          override = Override.command(declaration, attributes, block)
          extends_post = Options.extends_post_block(declaration, extends, callbacks)

          code =
            (types ++ default ++ implemented_by ++ extends_pre ++ override ++ extends_post)
            |> Macro.prewalk(&fix_location(&1, lines[command_id]))

          defined_callbacks = Macro.postwalk(code, [], &get_defs/2) |> elem(1)
          renamed_code = Macro.postwalk(code, &Override.rename_def_in_command(&1, command_id))

          [{command_id, %{code: renamed_code, callbacks: defined_callbacks}} | acc]
      end

    commands_code = Enum.map(commands_result, fn {_id, info} -> info.code end)

    commands_callbacks =
      Enum.map(commands_result, fn {command_id, info} ->
        {command_id, info.callbacks |> Enum.uniq()}
      end)

    info =
      Info.generate(
        env.module,
        attributes,
        attribute_init,
        attribute_type,
        commands,
        command_declaration,
        commands_callbacks
      )

    (state ++ commands_code)
    |> Macro.to_string()
    |> Code.format_string!()
    |> Logger.debug()

    behaviour ++ [info] ++ state ++ commands_code
  end

  ##############################################################################
  # helpers
  ##############################################################################

  defp fix_location({key, info, args}, line), do: {key, info ++ [line: line], args}
  defp fix_location(ast, _line), do: ast
end
